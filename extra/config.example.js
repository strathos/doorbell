module.exports = {
    "application": {
        "env":"DEV",
        "port":8088,
        "pincode":1234,
        "timeout":3
    },
    "blueiris": {
        "ip":"10.0.0.1",
        "port":"81",
        "canera":"CameraShortName",
        "user":"admin",
        "password":"admin"
    },
    "email": {
        "fromAddress":"sender@example.home",
        "toAddress":"first.last@example.com",
        "subjectPrefix":"Door bell pressed on ",
        "contentPrefix":"Door bell pressed on ",
        "sendGridApiKey":"create-and-paste-your-sendgrid-apikey-here"
    }
}
