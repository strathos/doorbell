$(function() {
    $.fn.numpad.defaults.gridTpl = '<table class="table modal-content"></table>';
    $.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
    $.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
    $.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn btn-default"></button>';
    $.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn" style="width: 100%;"></button>';
    $.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};
    $('#pincode').numpad({
        displayTpl: '<input class="form-control" type="password" />',
        hidePlusMinusButton: true,
        hideDecimalButton: true
    });

    $('#btnOpen').on('click', function() {
        var url = "http://" + window.location.host + "/api/lock/trigger";
        var pincodeInput = $('#pincode').val();
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                pincode: pincodeInput
            },
            error: function(data) {
                alert(data.responseText);
            }
        });
    });
});
