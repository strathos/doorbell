var config = require('./config');
var express = require('express');
var bodyParser = require('body-parser');
var Gpio = require('onoff').Gpio;
var moment = require('moment');
var logger = require('./app/logger');
var http = require('http');

var app = express();
var buzzer = new Gpio(17, 'out');
var manualTestButton = new Gpio(18, 'in', 'both');
var doorBellButton = new Gpio(23, 'in', 'falling');
var env = process.env.NODE_ENV || config.application.env;
var port = process.env.PORT || config.application.port;
var blueIrisIP = process.env.BLUEIRIS_IP || config.blueiris.ip;
var blueIrisPort = process.env.BLUEIRIS_PORT || config.blueiris.port;
var blueIrisCamera = process.env.BLUEIRIS_CAMERA || config.blueiris.camera;
var blueIrisUser = process.env.BLUEIRIS_USER || config.blueiris.user;
var blueIrisPassword = process.env.BLUEIRIS_PASS || config.blueiris.password;
var doorBellPushed = false;
var emailer = require('./app/emailer');

var http_options = {
    hostname: blueIrisIP,
    port: blueIrisPort,
    method: 'GET',
    path: '/admin?camera='+blueIrisCamera+'&trigger&user='+blueIrisUser+'&pw='+blueIrisPassword
}

manualTestButton.watch(function(err, value) {
    if (err) exit();
    buzzer.writeSync(value);
    if (value == 1) logger.info("Manual test button pushed.");
});

doorBellButton.watch(function(err, value) {
    if (err) exit();
    if (doorBellPushed == false) {
        logger.info("Door bell pushed.");
        logger.debug("Door bell timer started and click state set to true.");
        emailer(moment().format(), logger);

        var req = http.request(http_options, res => {
            logger.info("Camera trigger sent, status code from server: " + res.statusCode);
        })
        req.on('error', error => {
            logger.info("Camera trigger failer, error: " + error);
        })
        req.end();

        doorBellPushed = true;
        setTimeout(function() {
            logger.debug("Door bell timer timed out and click state set to false.");
            doorBellPushed = false;
        }, 10000);
    }
});

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/api', require('./routes/apiRoutes')(buzzer, logger));
app.use(express.static('public'));

app.listen(port, function() {
    logger.info("Server Running on " + port);
    logger.debug("Debug logs enabled.");
});

function exit() {
    buzzer.unexport();
    manualTestButton.unexport();
    doorBellButton.unexport();
    logger.info("Exit signal received, unexporting everything.");
    process.exit();
}

process.on('SIGINT' || 'SIGKILL', exit);
