#Door bell and/or electronic door lock opener with physical button and Web UI
Originally based on a tutorial by Arvind Ravulavaru: http://thejackalofjavascript.com/rpi-buzzer-node-iot-doorbell/
Later the project was migrated to use Express 4 framework.

This project was built and tested with Raspbian Jessie 2015-09-24 installed on the original Raspberry Pi B. Remember to run raspi-config to make the partition bigger and to set local timezone.

##Install Node.js 6.7.0

    mkdir ~/temp
    cd ~/temp
    wget http://nodejs.org/dist/latest-v6.x/node-v6.7.0-linux-armv6l.tar.gz
    tar xvzf node-v6.7.0-linux-armv6l.tar.gz
    sudo mkdir /opt/node
    sudo cp -r node-v6.7.0-linux-armv6l/* /opt/node
    sudo ln -s /opt/node/bin/node /usr/local/bin/node
    sudo ln -s /opt/node/bin/npm /usr/local/bin/npm
    rm -r node*
    cd ~
    sudo npm install -g node-gyp

##Install the door bell app

    cd ~
    git clone git@bitbucket.org:strathos/doorbell.git doorBell
    cd doorBell
    npm Install
    cp extra/config.example.js config.js
    nano config.js

Debian Jessie has SystemD, which can easily run Node.js apps without any helper app (Forever or Nodemon):

	sudo cp /home/pi/doorBell/extra/node-doorbell.service /etc/systemd/system/node-doorbell.service
    sudo nano /etc/systemd/system/node-doorbell.service
    sudo systemctl daemon-reload
    sudo systemctl start node-doorbell
    sudo systemctl enable node-doorbell

Schematics can be found from extra-directory, a file called Node.js-DoorBell.png, but also from [Schematics](http://www.schematics.com/project/nodejs-doorbell-40024/).

##Usage
###Basic Functionality
Pushing the push button will activate the LED (and open electric lock via a relay or a transistor). Web functionality is listening on http://localhost. The default port is 8088, but SystemD script sets port to 80. You can also set the port when running the app (PORT=8080 npm start). API is listening on http://localhost/api/lock/trigger. Authorization is a very simple pin code check. You can use for example curl (curl --data "pincode=4321" http://<server ip>:8088/api/lock/trigger). Pincode is also set in SystemD script. By default the pincode is 1234. Web client uses jQuery's POST call to trigger the led (and lock).

###Logging
Events are logged to doorBell/logs/doorBell.log but also to syslog via SystemD. Change NODE_ENV to debug to get full logging enabled.

###Email sending
The application uses Sendgrid to send email. [Sendgrid](https://sendgrid.com/) is free for small projects and very easy to use. Create an account and a then an API key. Copy-paste this API key to config.js together with all the other email configuration.
