var config = require('../config');
var sg = require('sendgrid')(process.env.SENDGRID_API_KEY || config.email.sendGridApiKey);

var request = null;

module.exports = function(timestamp, logger) {
    request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: {
            personalizations: [{
                to: [{
                    email: config.email.toAddress,
                }, ],
                subject: config.email.subjectPrefix + timestamp,
            }, ],
            from: {
                email: config.email.fromAddress,
            },
            content: [{
                type: 'text/plain',
                value: config.email.contentPrefix + timestamp,
            }, ],
        },
    });
    sg.API(request, function(error, response) {
        logger.debug(response.statusCode);
        logger.debug(response.headers);
    });
}
