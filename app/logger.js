var config = require('../config');
var winston = require('winston');
var fs = require('fs');
var moment = require('moment');
var env = process.env.NODE_ENV || config.application.env;

var timeStampFormat = function() {
    return moment().format();
}

if (!fs.existsSync('logs')) {
    fs.mkdirSync('logs');
}

var logger = new(winston.Logger)({
    transports: [
        new(winston.transports.Console)({
            timestamp: timeStampFormat,
            colorize: true,
            level: env === 'DEV' ? 'debug' : 'info'
        }),
        new(winston.transports.File)({
            filename: "logs/doorBell.log",
            timestamp: timeStampFormat,
            level: env === 'DEV' ? 'debug' : 'info'
        })
    ]
});

module.exports = logger;
