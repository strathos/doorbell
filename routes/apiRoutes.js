var config = require('../config');
var express = require('express');
var sleep = require('sleep');
var correctPincode = process.env.PINCODE || config.application.pincode;

var routes = function(buzzer, logger) {
    var apiRouter = express.Router();

    apiRouter.route('/lock/trigger')
    .get(function(req, res) {
        res.status(420).send('Use POST with pincode');
    })
    .post(function(req, res) {
        var triedPincode = req.body.pincode;
        if (triedPincode == correctPincode) {
            buzzer.writeSync(1);
            res.status(200).send("OK");
            logger.info("Lock opened.");
            sleep.sleep(config.application.timeout);
            buzzer.writeSync(0);
        } else {
            res.status(401).send("Incorrect pincode");
            logger.info("Incorrect pincode given (" + triedPincode + ").");
        };
    });

    return apiRouter;
};

module.exports = routes;
